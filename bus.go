package balancer

// шина данных для общения воркера с балансером
type Bus struct {
	IN   chan interface{} // воркер получает данне
	OUT  chan interface{} // воркер посылает данные
	ERR  chan error       // воркер посылает ошибки
	DEAD chan error       // воркер говорит что сдох
	DONE chan struct{}    // воркер получает команду умереть
}
