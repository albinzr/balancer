package balancer

import (
	"fmt"
	"io"
	"log"
	"strings"
	"time"
)

type Logger struct {
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
}

// конфигурация балансера
type Config struct {
	Min                 int             // минимальное количество воркеров
	Max                 int             // максимальное количество воркеров
	Interval            time.Duration   // интервал опроса состояния балансера для регулироания кол-ва воркеров
	Level               int             // кол-во задач в канале для управления кол-вом воркеров
	Buffer              int             // размер буфера каналов внешнего интерфейса
	UseExternalHandlers bool            // использовать внешний интерфейс для передачи результата и ошибок
	Worker              WorkerInterface // интерфейс реализующий функцию work(), ее будет запускать балансер
	Logger              *Logger         // интерфейс для логирования
}

// создает логгер
func MakeLogger(name string, infoWriter io.Writer, warningWriter io.Writer, errorWriter io.Writer) *Logger {
	return &Logger{
		Info:    log.New(infoWriter, makePrefix("INFO", name), log.Ldate|log.Ltime),
		Warning: log.New(warningWriter, makePrefix("WARNING", name), log.Ldate|log.Ltime|log.Llongfile),
		Error:   log.New(errorWriter, makePrefix("ERROR", name), log.Ldate|log.Ltime|log.Llongfile),
	}
}

func makePrefix(prefix string, name string) string {
	return fmt.Sprintf("%v: [%v]:\t", prefix, strings.ToUpper(name))
}
