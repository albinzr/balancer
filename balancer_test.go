package balancer

import (
	"math/rand"
	"os"
	"testing"
	"time"
)

func TestNewBalancer(t *testing.T) {
	worker := new(testWorker)

	c := Config{
		Min:      3,
		Max:      15,
		Interval: time.Duration(1 * time.Second),
		Level:    20,
		Buffer:   200,
		Worker:   worker,
		Logger:   MakeLogger(os.Stdout, os.Stdout, os.Stderr),
	}

	balancer := NewBalancer(c)
	iface := balancer.GetExternalInterface()
	go balancer.Run()

	for {
		iface.InCh <- "message"
		time.Sleep(time.Duration(rand.Intn(6)) * time.Millisecond)
	}
}

func TestBalancer_Run(t *testing.T) {

}

func TestBalancer_GetExternalInterface(t *testing.T) {

}

type testWorker struct {
}

func (w *testWorker) work(bus *Bus) {
	for {
		select {
		case <-bus.DONE:
			return
		default:
			<-bus.IN

			if rand.Intn(1671)%1112 == 0 {
				panic("panic in worker")
			}

			time.Sleep(time.Duration(rand.Intn(20)) * time.Millisecond)
		}
	}

}
