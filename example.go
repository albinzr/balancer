package balancer

import (
	"fmt"
	"time"
)

type ExampleWorker struct {
	WorkerInterface
}

func main() {
	w := new(ExampleWorker)
	c := Config{
		Min:      1,
		Max:      5,
		Interval: time.Duration(2 * time.Second),
		Level:    100,
		Buffer:   200,
		Worker:   w,
	}
	b := NewBalancer(c)

	go b.Run()

	bus := b.getBus()

	for i := 0; i < 100; i++ {
		bus.IN <- i
	}
}

func (w *ExampleWorker) work(bus *Bus) {
	for {
		select {
		case <-bus.DONE:
			return
		case data := <-bus.IN:
			msg, ok := data.(int)
			if !ok {
				fmt.Errorf("incorrect message type")
			}

			fmt.Printf("Message %#v", msg)
		}
	}
}
