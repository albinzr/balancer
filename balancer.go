package balancer

import (
	"github.com/felixge/pidctrl"
	"sync/atomic"
	"time"
)

// внешний интерфейс
type ExternalInterface struct {
	InCh  chan interface{} // сюда поступают данные для обработки
	OutCh chan interface{} // сюда попадают результаты выполнения обработки
	ErrCh chan error       // сюда попадают ошибки обработки
}

// внутренний интерфейс
type InternalInterface struct {
	DoneCh chan struct{} // балансер кричит сюда когда хочет убрать один воркер
	DeadCh chan error    // воркер пукает сюда когда помирает
}

// сам балансер
type Balancer struct {
	Config Config            // конфигурации балансера
	Cnt    int32             // количество запущенных воркеров
	II     InternalInterface // внутренний интерфейс
	EI     ExternalInterface // внешний интерфейс
}

// создает и возвращает шину для обмена данными воркера с балансировщиком
func (b *Balancer) getBus() *Bus {
	return &Bus{
		IN:   b.EI.InCh,
		OUT:  b.EI.OutCh,
		ERR:  b.EI.ErrCh,
		DONE: b.II.DoneCh,
		DEAD: b.II.DeadCh,
	}
}

// обертка для перехвата паники
func (b *Balancer) work() {
	bus := b.getBus()
	defer PanicRecover(bus)
	b.Config.Worker.work(bus)
}

// для запуска функции под руководством балансера
func (b *Balancer) addWorker() {
	go b.work()
	b.Config.Logger.Info.Println("add worker")
	atomic.AddInt32(&b.Cnt, 1)
}

// посылает сигнал на завершение одной функции
func (b *Balancer) killWorker() {
	b.II.DoneCh <- struct{}{}
	b.Config.Logger.Info.Println("kill worker")
	atomic.AddInt32(&b.Cnt, -1)
}

// возвращает интерфейс для передачи и получения данных из канала
func (b *Balancer) GetExternalInterface() *ExternalInterface {
	return &b.EI
}

// запуск балансера
func (b *Balancer) Run() {
	for i := 0; i < b.Config.Min; i++ {
		b.addWorker()
	}

	go func() {
		for err := range b.II.DeadCh {
			b.Config.Logger.Error.Println("worker is dead:", err.Error())
			atomic.AddInt32(&b.Cnt, -1)
		}
	}()

	// TODO отладить грамотную балансировку нагрузки
	// TODO метрики
	// отрицательные значения, так как реверсивное управление
	pid := pidctrl.NewPIDController(-0.03, -0.01, -0.01)
	pid.Set(float64(b.Config.Level))
	pid.SetOutputLimits(float64(b.Config.Min), float64(b.Config.Max))

	ticker := time.Tick(b.Config.Interval)

	for _ = range ticker {
		length := len(b.EI.InCh)
		cnt := atomic.LoadInt32(&b.Cnt)
		r := pid.Update(float64(length))

		abs := int(r) - int(cnt)

		switch {
		case abs < 0:
			for i := 0; i < -abs; i++ {
				b.killWorker()
			}
		case abs > 0:
			for i := 0; i < abs; i++ {
				b.addWorker()
			}
		}
	}
}

// создание нового балансера
func NewBalancer(c Config) (b *Balancer) {
	b = &Balancer{
		Config: c,
		Cnt:    0,
		EI: ExternalInterface{
			InCh:  make(chan interface{}, c.Buffer),
			OutCh: make(chan interface{}, c.Buffer),
			ErrCh: make(chan error, c.Buffer),
		},
		II: InternalInterface{
			DoneCh: make(chan struct{}, c.Max),
			DeadCh: make(chan error, c.Max),
		},
	}

	return b
}

// возвращает количество запущенных воркеров
func (b *Balancer) GetWorkersCount() interface{} {
	return atomic.LoadInt32(&b.Cnt)
}

// возвращает количество заданий в канале
func (b *Balancer) GetChannelLength() interface{} {
	return len(b.EI.InCh)
}
