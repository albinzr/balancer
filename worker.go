package balancer

import "fmt"

// представляет собой интерфейс для передачи балансеру
type WorkerInterface interface {
	// принимает шину данных для взаимодействия с балансером
	work(bus *Bus)
}

// вызываю в defer воркера, чтобы сказать балансеру о смерти
func PanicRecover(bus *Bus) {
	if r := recover(); r != nil {
		bus.DEAD <- fmt.Errorf("worker recover with - %v", r)
	}
}
